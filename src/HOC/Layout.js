import React from 'react'
import HeaderTheme from '../component/HeaderTheme/HeaderTheme'
import Footer from '../component/HeaderTheme/Footer'
export default function Layout({ Component }) {
    return (
        <div>
            <HeaderTheme />
            <Component />
            <Footer />
        </div>
    )
}
