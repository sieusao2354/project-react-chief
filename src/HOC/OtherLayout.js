import React from 'react'
import Footer from '../component/HeaderTheme/Footer'
import OtherHeaderTheme from '../component/HeaderTheme/OtherHeaderTheme'

export default function OtherLayout({ Component1 }) {
    return (
        <div>
            <OtherHeaderTheme />
            <Footer />
        </div>
    )
}
