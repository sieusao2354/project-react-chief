import React from 'react'
import "./CssFooter.css"
export default function Footer() {

    let renderFooterChief = () => {
        return (
            <div className='bg-black footerContainer' >
                <div className='flex text-white px-32 pt-32 ' >
                    <div className=' footerField px-10'>
                        <div className='flex cursor-pointer footerText '  >
                            <p className='text-orange-500' >POULTRY </p>
                            <p>FIELD</p>
                        </div>
                        <div>
                            <p>Lorem ipsum dolor sit, amet consectetur elit. Earum mollitia ipsam autem ipsam.dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.</p>
                        </div>
                        <div className='flex mt-10 ' >
                            <div className='footerIcon' >
                                <i class="fa-brands fa-facebook-f"></i>
                            </div>
                            <div className='footerIcon'  >
                                <i class="fa-brands fa-instagram"></i>
                            </div>
                            <div className='footerIcon' >
                                <i class="fa-brands fa-twitter"></i>
                            </div>
                            <div className=' footerIcon' >
                                <i class="fa-brands fa-google-plus-g"></i>
                            </div>
                        </div>
                    </div>
                    <p className='px-24 footerText' >Links</p>
                    <p className='px-20 footerText'>Services</p>
                    <p className='px-20 footerText' > Instagram Feed</p>
                </div>
                <div className=''>
                    <p className='copyRight'>  © 2023 Poultry Field. All Rights Reserved | WordPress Theme by W3Layouts.</p>
                </div>
            </div>
        )
    }


    return (
        <div>
            {renderFooterChief()}
        </div>
    )
}
