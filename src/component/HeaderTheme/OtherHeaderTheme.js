import React from 'react'
import "./OtherHeaderTheme.css"
export default function OtherHeaderTheme() {
    return (
        <div>
            <img className='img404' src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/banner6.jpg" alt="" />

            <div  >
                <p className='Err' >404</p>
                <p className='OopsErr' >Oops, Page not found!</p>
                <p className='text-Err' >Sorry, we're offline right now to make our site even better. Please, <br /> comeback later and check what we've been upto.</p>
                <div className='flex'>
                    <div className='inputErr ' >
                        <input placeholder='Enter your text' />
                    </div>
                    <div className='inputErr1' >
                        <i class="fa-solid fa-magnifying-glass"></i>
                    </div>
                </div>
                <a className='backto-Err  ' onClick={() => {
                    window.location.href = "/"
                }}>
                    <i class="fa-sharp fa-solid fa-arrow-left"></i>
                    BACK TO HOME</a>
            </div>
        </div >
    )
}
