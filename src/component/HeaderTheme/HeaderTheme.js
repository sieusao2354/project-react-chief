import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import "./CssHeader.css"
import React from 'react';
import { DownOutlined } from '@ant-design/icons';
import { Dropdown, Space } from 'antd';
export default function CarouselMovie() {

    const responsive = {
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 1,
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 1,
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1,
        }
    };
    const items = [
        {
            label: <a onClick={() => {
                window.location.href = "blogpage"
            }} >Blog Pages</a>,
            key: '0',
        },
        {
            label: <a onClick={() => {
                window.location.href = "404err"
            }}>404 page</a>,
            key: '1',
        },
        {
            type: 'divider',
        },

    ];
    const App = () => (
        <Dropdown
            menu={{
                items,
            }}
            trigger={['click']}
        >
            <a onClick={(e) => e.preventDefault()}>
                <Space>
                    Click me
                    <DownOutlined />
                </Space>
            </a>
        </Dropdown>
    );



    let renderCarouselMenu = () => {
        return (

            <div style={{ position: 'fixed' }} className=" bg-black w-full header-nav "   >
                <div className="flex" >
                    <div className="flex px-24 py-10 font-medium text-3xl header-main " onClick={() => {
                        window.location.href = "/"
                    }}  >
                        <a className="px-1 text-orange-500 nameDetail " >   POULTRY </a>
                        <a className="text-white nameDetail " >FIELD</a>
                    </div>
                    <div className="flex py-10 font-medium px-20 text-white" >

                        {/* <div className="nav-mobie">
                            <i class="fa-solid fa-bars"></i>

                        </div> */}


                        <div className="nav-PC flex " >
                            <a onClick={() => {
                                window.location.href = "/"
                            }} className="px-10 hover:text-orange-500 Menu text-orange-500">Home</a>
                            <a onClick={() => {
                                window.location.href = "About"
                            }} className="Menu  hover:text-orange-500" >About</a>
                            <a onClick={() => {
                                window.location.href = "service"
                            }} className="px-10 hover:text-orange-500 Menu">Services</a>


                            <Dropdown
                                menu={{
                                    items,
                                }}
                                trigger={['click']}
                            >
                                <a onClick={(e) => e.preventDefault()}>
                                    <Space>
                                        Pages
                                        <DownOutlined />
                                    </Space>
                                </a>
                            </Dropdown>

                            {/* <a className="px-10 hover:text-orange-500 Menu" >Contact</a> */}

                        </div>

                    </div>
                </div>
                {/* 
                <div className="nav-overlay"></div>
                <div className="nav_mobie" >
                    <div className="times-nav">
                        <i class="fa-solid fa-xmark"></i>
                    </div>
                    <a onClick={() => {
                        window.location.href = "/"
                    }} className="p-10 hover:text-orange-500 Menu-mobile text-orange-500">Home</a>
                    <br />
                    <br />
                    <a onClick={() => {
                        window.location.href = "About"
                    }} className="Menu-mobile p-10 hover:text-orange-500" >About</a>
                    <br />
                    <br />
                    <a onClick={() => {
                        window.location.href = "service"
                    }} className="p-10 hover:text-orange-500 Menu-mobile">Services</a><br /><br />


                    <Dropdown
                        menu={{
                            items,
                        }}
                        trigger={['click']}
                    >
                        <a className="p-10" onClick={(e) => e.preventDefault()}>
                            <Space>
                                Pages
                                <DownOutlined />
                            </Space>
                        </a>

                    </Dropdown><br /><br />

                    <a className="p-10 hover:text-orange-500 Menu-mobile" >Contact</a>



                </div> */}
            </div>


        )

    }


    let renderCarouselMovie = () => {

        return (
            <div className="" >


                < Carousel
                    swipeable={false}
                    draggable={false}
                    showDots={true}
                    responsive={responsive}
                    ssr={false} // means to render carousel on server-side.
                    infinite={true}
                    // autoPlay={this.props.deviceType !== "mobile" ? true : false}
                    autoPlay={2000}
                    autoPlaySpeed={4000}
                    keyBoardControl={true}
                    // transitionDuration={1000}
                    containerClass="carousel-container"
                    removeArrowOnDeviceType={["tablet", "mobile"]}
                // deviceType={this.props.deviceType}
                >

                    <div>
                        <div className='bg-black' >


                            <img className='h-full w-full imgCarousel' src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/banner1.jpg" alt="" />
                        </div>
                        <div className="detailCarousel1 text-gray-300 px-14 font-medium  text-sx" > <p>Special Every day </p></div>
                        <div className="text-white detailCarousel2 px-14 font-medium" ><p>The Chickens Always</p>
                            <p className="text-white detailCarousel3  font-medium" >Comes Chief.</p>
                        </div>
                        <div className="px-24 "  >
                            <button className="buttonCarousel" >Read More</button>

                        </div>
                    </div>
                    <div>
                        <div className='bg-black' >


                            <img className='h-full w-full imgCarousel' src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/banner2.jpg" alt="" />
                        </div>
                        <div className="detailCarousel1 text-gray-300 px-14 font-medium  text-sx" > <p>Special Every day</p></div>
                        <div className="text-white detailCarousel2 px-14 font-medium" ><p>Natures Intention</p>
                            <p className="text-white detailCarousel3  font-medium" >With Chickens.</p>
                        </div>
                        <div className="px-24 "  >
                            <button className="buttonCarousel" >Read More</button>

                        </div>
                    </div>
                    <div>
                        <div className='bg-black' >


                            <img className='h-full w-full imgCarousel' src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/banner3.jpg" alt="" />
                        </div>
                        <div className="detailCarousel1 text-gray-300 px-14 font-medium  text-sx" > <p>Special Every day</p></div>
                        <div className="text-white detailCarousel2 px-14 font-medium" ><p>The Chickens Always</p>
                            <p className="text-white detailCarousel3  font-medium" >Comes Chief.</p>
                        </div>
                        <div className="px-24 "  >
                            <button className="buttonCarousel" >Read More</button>

                        </div>
                    </div>
                    <div>
                        <div className='bg-black' >


                            <img className='h-full w-full imgCarousel' src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/banner2.jpg" alt="" />
                        </div>
                        <div className="detailCarousel1 text-gray-300 px-14 font-medium  text-sx" > <p>Special Every day</p></div>
                        <div className="text-white detailCarousel2 px-14 font-medium" ><p>Natures Intention</p>
                            <p className="text-white detailCarousel3  font-medium" >With Chickens.</p>
                        </div>
                        <div className="px-24 "  >
                            <button className="buttonCarousel" >Read More</button>

                        </div>
                    </div>




                </Carousel >



            </div >
        )
    }
    return (

        <div>
            <div className="menuCarousel" >
                {renderCarouselMenu()}
            </div>


            <div className="carouselHeader" >
                {renderCarouselMovie()}
            </div>


        </div>
    )
}
