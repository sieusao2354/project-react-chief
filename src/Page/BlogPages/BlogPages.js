import React from 'react'
import "./blogPage.css"
export default function BlogPages() {


    let renderBlogPost = () => {
        return (
            <div className='flex backgroundBlogPost ' >
                <div className=' ' >
                    <div className='flex img-text-blog ' >
                        <div className='img-blog' >
                            <img src="https://wp.w3layouts.com/poultry-field/wp-content/uploads/sites/78/2022/03/g6.jpg" alt="" />
                            <p className='day-month-year-blog' >24-03-2023</p>
                            <div className=' flex icon-text-w3-blog py-2' >
                                <div className='' >
                                    <i class="fa-solid fa-user "></i>
                                </div>
                                <p className='text-gray-400 px-2 text-lg ' >By </p>
                                <p className='w3-blog font-bold ' >W3Layouts</p>

                            </div>
                            <p className='helloWorld-blog' >Hello world!</p>
                            <button className='button-text-img-Blog' >
                                Read Me
                            </button>
                        </div>
                        <div className='img-blog' >
                            <img src="https://wp.w3layouts.com/poultry-field/wp-content/uploads/sites/78/2022/03/g5.jpg" alt="" />
                            <p className='day-month-year-blog' >24-03-2023</p>
                            <div className=' flex icon-text-w3-blog py-2' >
                                <div className='' >
                                    <i class="fa-solid fa-user "></i>
                                </div>
                                <p className='text-gray-400 px-2 text-lg ' >By </p>
                                <p className='w3-blog font-bold ' >W3Layouts</p>

                            </div>
                            <p className='helloWorld-blog' >
                                Making The Offshore Life Cycle Smarter</p>

                            <button className='button-text-img-Blog' >
                                Read Me
                            </button>
                        </div>

                    </div>
                    <div className='flex img-text-blog ' >
                        <div className='img-blog' >
                            <img src="https://wp.w3layouts.com/poultry-field/wp-content/uploads/sites/78/2022/03/g1.jpg" alt="" />
                            <p className='day-month-year-blog' >24-03-2023</p>
                            <div className=' flex icon-text-w3-blog py-2' >
                                <div className='' >
                                    <i class="fa-solid fa-user "></i>
                                </div>
                                <p className='text-gray-400 px-2 text-lg ' >By </p>
                                <p className='w3-blog font-bold ' >W3Layouts</p>

                            </div>
                            <p className='helloWorld-blog' >Highly Professional Staff</p>
                            <button className='button-text-img-Blog' >
                                Read Me
                            </button>
                        </div>
                        <div className='img-blog' >
                            <img src="https://wp.w3layouts.com/poultry-field/wp-content/uploads/sites/78/2022/03/g2.jpg" alt="" />
                            <p className='day-month-year-blog' >24-03-2023</p>
                            <div className=' flex icon-text-w3-blog py-2' >
                                <div className='' >
                                    <i class="fa-solid fa-user "></i>
                                </div>
                                <p className='text-gray-400 px-2 text-lg ' >By </p>
                                <p className='w3-blog font-bold ' >W3Layouts</p>

                            </div>
                            <p className='helloWorld-blog' >
                                Making The Offshore Life Cycle Smarter</p>

                            <button className='button-text-img-Blog' >
                                Read Me
                            </button>
                        </div>

                    </div>
                    <div className='flex img-text-blog ' >
                        <div className='img-blog' >
                            <img src="https://wp.w3layouts.com/poultry-field/wp-content/uploads/sites/78/2022/03/g5.jpg" alt="" />
                            <p className='day-month-year-blog' >21-09-2022</p>
                            <div className=' flex icon-text-w3-blog py-2' >
                                <div className='' >
                                    <i class="fa-solid fa-user "></i>
                                </div>
                                <p className='text-gray-400 px-2 text-lg ' >By </p>
                                <p className='w3-blog font-bold ' >W3Layouts</p>

                            </div>
                            <p className='helloWorld-blog' >Accurate Testing Processes</p>
                            <button className='button-text-img-Blog' >
                                Read Me
                            </button>
                        </div>
                        <div className='img-blog' >
                            <img src="https://wp.w3layouts.com/poultry-field/wp-content/uploads/sites/78/2022/03/g6.jpg" alt="" />
                            <p className='day-month-year-blog' >01-07-2019</p>
                            <div className=' flex icon-text-w3-blog py-2' >
                                <div className='' >
                                    <i class="fa-solid fa-user "></i>
                                </div>
                                <p className='text-gray-400 px-2 text-lg ' >By </p>
                                <p className='w3-blog font-bold ' >W3Layouts</p>

                            </div>
                            <p className='helloWorld-blog' >
                                Making The Offshore Life Cycle Smarter</p>

                            <button className='button-text-img-Blog' >
                                Read Me
                            </button>
                        </div>

                    </div>

                    <div></div>
                </div>


                <div>
                    <div className='flex search-blog'  >
                        <div className='search-blog1' >
                            <input type="search" placeholder='Sreach ...' />
                        </div>
                        <div className='search-blog2' >
                            <p>Search</p>

                        </div>

                    </div>

                    <div className='RencentPost' >
                        <p className='rencent' >Recent Posts</p>
                        <div className='helloRencent' >
                            <a href="">Hello world!</a>

                        </div>
                        <div className='helloRencent' >
                            <a href="">Making The Offshore Life Cycle Smarter</a>

                        </div>
                        <div className='helloRencent' >
                            <a href="">Highly Professional Staff, Accurate Testing Processes</a>

                        </div>
                        <div className='helloRencent' >
                            <a href="">Making The Offshore Life Cycle Smarter</a>

                        </div>
                        <div className='helloRencent' >
                            <a href="">Highly Professional Staff, Accurate Testing Processes</a>

                        </div>
                    </div>
                    <div className='rencentComment' >
                        <p className='rencent' >Recent Comments</p>
                        <div className='py-3'>
                            <div className='flex' >
                                <a className="text-orange-500 hover:text-orange-500" href="">A WordPress Commenter</a>
                                <p className='px-2' >on</p>
                            </div>
                            <a className="text-orange-500 hover:text-orange-500" href="">Hello world!</a>
                        </div>
                        <div className='py-2'>
                            <div className='flex' >
                                <a className="text-orange-500 hover:text-orange-500" href="">A WordPress Commenter</a>
                                <p className='px-2' >on</p>
                            </div>
                            <a className="text-orange-500 hover:text-orange-500" href="">Hello world!</a>
                        </div>

                    </div>
                    <div className='recentArchives' >
                        <p className='rencent' >Achives</p>


                        <div className='flex text-icon-Achives' >
                            <div className=' iconaArchives' >
                                <i class="fa-solid fa-user"></i>
                            </div>
                            <div className='' >
                                <p className='textAchives' >March 2022</p>
                            </div>
                        </div>
                        <div className='flex text-icon-Achives' >
                            <div className=' iconaArchives' >
                                <i class="fa-solid fa-user"></i>
                            </div>
                            <div>
                                <p className='textAchives' >February 2022</p>
                            </div>
                        </div>
                        <div className='flex text-icon-Achives' >
                            <div className=' iconaArchives' >
                                <i class="fa-solid fa-user"></i>
                            </div>
                            <div>
                                <p className='textAchives' >January 2022</p>
                            </div>
                        </div>
                        <div className='flex text-icon-Achives' >
                            <div className=' iconaArchives' >
                                <i class="fa-solid fa-user"></i>
                            </div>
                            <div>
                                <p className='textAchives' >December 2021
                                </p>
                            </div>
                        </div>

                    </div>

                    <div className='recentMeta' >
                        <p className='rencent' >Meta</p>
                        <div className='helloRencent' >
                            <a href="">Log in</a>
                        </div>
                        <div className='helloRencent' >
                            <a href="">Entries feed</a>
                        </div>
                        <div className='helloRencent' >
                            <a href="">Comments feed</a>
                        </div>
                        <div className='helloRencent' >
                            <a href="">WordPress.org</a>
                        </div>
                    </div>
                </div>


            </div>
        )
    }


    return (
        <div>
            {renderBlogPost()}

        </div>
    )
}
