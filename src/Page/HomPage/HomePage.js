import React from "react";
import "./CssHomePage.css";


export default function HomePage() {



    let renderHealthy = () => {
        return (
            <div className="flex ">
                <div className="healthy">
                    <img
                        className="healthyImg"
                        src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/g2.jpg"
                        alt=""
                    />
                </div>
                <div className="textHealthy">
                    <p className="getText">GET RATHER HEALTHY.</p>
                    <p className="textField font-medium">Welcome To Our Poultry Field.</p>
                    <p className="textlorem">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br />
                        Aut tempore quod doloribus accusamus perferendis, <br /> autem
                        facilis. Illum, quidem, nesciunt eveniet solu, <br /> libero vel
                        eum, ipsam doloremque totam Lorem.
                    </p>
                    <div className="text-black">
                        <button className="GetButton">Read More</button>
                    </div>
                </div>
            </div>
        );
    };

    let renderWhatWeDo = () => {
        return (
            <div className="WhatWeDo mt-20">
                <div className="OurWhat pt-20">
                    <p className="textWhat">WHAT WE DO</p>
                    <p className="textOur">Our Services</p>
                </div>
                <div className="services flex text-center">
                    <div className="serviceDetail">
                        <i className="fa-solid fa-shield-halved iconService"></i>
                        <p className="textQuality">Quality Matters</p>
                        <p className="textLoremQuality mt-4">
                            Lorem ipsum dolor sit amet, elit. Id <br /> ab commodi magnam.
                        </p>
                    </div>
                    <div className="serviceDetail">
                        <i class="iconService fa-solid fa-truck-fast"></i>
                        <p className="textQuality">Free Shipping</p>
                        <p className="textLoremQuality mt-4">
                            Lorem ipsum dolor sit amet, elit. Id <br /> ab commodi magnam.
                        </p>
                    </div>{" "}
                    <div className="serviceDetail">
                        <i class="iconService fa-solid fa-user-shield"></i>
                        <p className="textQuality">Customer Satisfaction</p>
                        <p className="textLoremQuality mt-4">
                            Lorem ipsum dolor sit amet, elit. Id <br /> ab commodi magnam.
                        </p>
                    </div>
                </div>
            </div>
        );
    };
    let renderInvention = () => {
        return (
            <div className="flex invention">
                <div className="inventionText">
                    <p className="inventionText1">OUR INVENTION</p>
                    <p className="inventionText2">Our Invention Is Always The Best</p>
                    <p className="inventionText3">
                        Lorem ipsum viverra feugiat. Pellen tesque libero ut justo, <br />{" "}
                        ultrices in ligula. Semper at tempufddfel.Lorem ipsum dolor <br />{" "}
                        sit, amet consectetur elit. Earum mollitia cum ex ipsam <br />{" "}
                        autem!earum sequi amet.
                    </p>
                    <button className="inventionButton">Read More</button>
                </div>
                <div className="breeder">
                    <div className="breederIconText flex">
                        <div>
                            <i class="breederIcon fa-brands fa-buromobelexperte"></i>
                        </div>
                        <div className="mt-10 ml-6 ">
                            <p className="breederText1">Breeder Management</p>
                            <p className="breederText2">
                                Lorem ipsum viverra feugiat. Pellen tesque ut justo, ultrices in
                                ligula tempufddfel feugiat .
                            </p>
                        </div>
                    </div>
                    <br />
                    <br />
                    <div className="breederIconText flex">
                        <div>
                            <i class=" breederIcon2 fa-solid fa-hands-asl-interpreting"></i>
                        </div>
                        <div className="mt-10 ml-6 ">
                            <p className="breederText1">Residue Treatment</p>
                            <p className="breederText2">
                                Lorem ipsum viverra feugiat. Pellen tesque ut justo, ultrices in
                                ligula tempufddfel feugiat .
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    let renderGallery = () => {
        return (
            <div className="gallery">
                <div>
                    <p className=" latest1 text-center font-bold">GALLERY</p>
                    <p className="text-center latest2">Latest Gallery</p>
                </div>
                <div className=" p-32 grid grid-cols-3 gap-6 w-full h-full ">
                    <div className="showcase-all">

                        <img className="picC" src="../img/c1.png" alt="" />

                        <div className="showcase-overlay" >
                            <div className="showcase-search">
                                <a data-fancybox="img" href="../img/c1.png">
                                    <i class="fa-brands fa-searchengin"></i></a>
                            </div>
                        </div>

                    </div>
                    <div className="showcase-all">

                        <img className="picC" src="../img/c2.png" alt="" />

                        <div className="showcase-overlay" >
                            <div className="showcase-search">
                                <a data-fancybox="img" href="../img/c2.png">
                                    <i className="fa-brands fa-searchengin" />
                                </a>

                            </div>
                        </div>

                    </div><div className="showcase-all">

                        <img className="picC" src="../img/c3.png" alt="" />

                        <div className="showcase-overlay" >
                            <div className="showcase-search">
                                <a data-fancybox="img" href="../img/c3.png">
                                    <i className="fa-brands fa-searchengin" /></a>
                            </div>
                        </div>

                    </div><div className="showcase-all">

                        <img className="picC" src="../img/c4.png" alt="" />

                        <div className="showcase-overlay" >
                            <div className="showcase-search">
                                <a data-fancybox="img" href="../img/c4.png">
                                    <i className="fa-brands fa-searchengin" /></a>
                            </div>
                        </div>

                    </div><div className="showcase-all">

                        <img className="picC" src="../img/c5.png" alt="" />

                        <div className="showcase-overlay" >
                            <div className="showcase-search">
                                <a data-fancybox="img" href="../img/c5.png">
                                    <i className="fa-brands fa-searchengin" /></a>
                            </div>
                        </div>

                    </div><div className="showcase-all">

                        <img className="picC" src="../img/c6.png" alt="" />

                        <div className="showcase-overlay" >
                            <div className="showcase-search">
                                <a data-fancybox="img" href="../img/c6.png">
                                    <i className="fa-brands fa-searchengin" /></a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        );
    };

    let renderCarouselContact = () => {
        return (



            <div>

            </div>
        )
    }













    return (
        <div>
            {renderHealthy()}
            {renderWhatWeDo()}
            {renderInvention()}
            {/* {renderGallery()} */}
            {/* {renderCarouselContact()} */}
        </div>
    );

}