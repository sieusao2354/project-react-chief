import React from 'react'
import "./Services.css"

export default function ServicePage() {

    let renderServiceWhatWeDo = () => {
        return (
            <div className='flex' >
                <div className='imgWhatWeDo-service' >
                    <img src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/ab4.jpg" alt="" />
                </div>
                <div className='text1-Service' >
                    <p className='WWDservice' >WHAT WE DO</p>
                    <p className='FieldService' >We Give You Complete <br /> Control Of Your Poultry <br /> Fields.</p>
                    <p className='LoremService' > Lorem ipsum viverra feugiat. Pellen tesque libero ut justo, <br /> ultrices in ligula. Semper at tempufddfel.Lorem ipsum dolor sit, <br /> amet consectetur elit. Earum mollitia cum ex ipsam autem!earum <br /> sequi amet.</p>

                    <div className='flex'>
                        <div>
                            <div className=' flex pt-2' >
                                <i className="fa-solid fa-check text-orange-500 px-2 text-xl " />

                                <p className='text-lg font-bold' > Fresh Chiken</p>
                            </div>
                            <div className=' flex pt-2' >
                                <i className="fa-solid fa-check text-orange-500 px-2 text-xl " />

                                <p className='text-lg font-bold' >Brown Eggs</p>
                            </div>
                            <div className=' flex pt-2' >
                                <i className="fa-solid fa-check text-orange-500 px-2 text-xl  " />

                                <p className='text-lg font-bold' > 24/7 Hours Support</p>
                            </div>


                        </div>
                        <div className='px-20'>
                            <div className=' flex pt-2' >
                                <i className="fa-solid fa-check text-orange-500 px-2 text-xl " />

                                <p className='text-lg font-bold' > Fresh Chiken</p>
                            </div>
                            <div className=' flex pt-2' >
                                <i className="fa-solid fa-check text-orange-500 px-2 text-xl " />

                                <p className='text-lg font-bold' >Brown Eggs</p>
                            </div>
                            <div className=' flex pt-2' >
                                <i className="fa-solid fa-check text-orange-500 px-2 text-xl  " />

                                <p className='text-lg font-bold' > 24/7 Hours Support</p>
                            </div>


                        </div>

                    </div>
                    <button className='Button-WhatWeDo-Service' >Read Me</button>
                </div>


            </div>
        )
    }
    let renderOurService = () => {
        return (
            <div className='offer-service' >
                <div className='text-center py-20 ' >
                    <p className='text-gray-600 font-bold' >OUR SERVICES
                    </p>
                    <p className=' text-Offer' >What We Offer
                    </p>
                </div>
                <div className='grid grid-cols-3 gap-10 px-10 '>
                    <div className='flex bg-grid-offer ' >
                        <div className='egg-icon-serv '>
                            <i class="fa-solid fa-egg"></i>
                        </div>
                        <div className='icon-text-offer-serv' >
                            <p className='text-egg-service' >
                                Alternative Egg</p>
                            <p className=' text-gray-600 text-lg our-lorem-service' >Lorem ipsum dolor sit amet, elit. Id ab commodi magnam.</p>
                        </div>
                    </div>

                    <div className='flex bg-grid-offer ' >
                        <div className='egg-icon-serv '>
                            <i class="fa-solid fa-warehouse"></i>
                        </div>
                        <div className='icon-text-offer-serv' >
                            <p className='text-egg-service' >

                                Poultry Cages</p>
                            <p className=' text-gray-600 text-lg our-lorem-service' >Lorem ipsum dolor sit amet, elit. Id ab commodi magnam.</p>
                        </div>
                    </div>
                    <div className='flex bg-grid-offer ' >
                        <div className='egg-icon-serv '>
                            <i class="fa-solid fa-kiwi-bird"></i>
                        </div>
                        <div className='icon-text-offer-serv' >
                            <p className='text-egg-service' >
                                Breeder Management</p>
                            <p className=' text-gray-600 text-lg our-lorem-service' >Lorem ipsum dolor sit amet, elit. Id ab commodi magnam.</p>
                        </div>
                    </div>
                    <div className='flex bg-grid-offer ' >
                        <div className='egg-icon-serv '>
                            <i class="fa-solid fa-feather"></i>
                        </div>
                        <div className='icon-text-offer-serv' >
                            <p className='text-egg-service' >
                                Poultry Climate</p>
                            <p className=' text-gray-600 text-lg our-lorem-service' >Lorem ipsum dolor sit amet, elit. Id ab commodi magnam.</p>
                        </div>
                    </div>
                    <div className='flex bg-grid-offer ' >
                        <div className='egg-icon-serv '>
                            <i class="fa-solid fa-hands"></i>
                        </div>
                        <div className='icon-text-offer-serv' >
                            <p className='text-egg-service' >

                                Residue Teatment</p>
                            <p className=' text-gray-600 text-lg our-lorem-service' >Lorem ipsum dolor sit amet, elit. Id ab commodi magnam.</p>
                        </div>
                    </div>
                    <div className='flex bg-grid-offer ' >
                        <div className='egg-icon-serv '>
                            <i class="fa-sharp fa-solid fa-fan"></i>
                        </div>
                        <div className='icon-text-offer-serv' >
                            <p className='text-egg-service' >
                                Exhaust Air Treatment</p>
                            <p className=' text-gray-600 text-lg our-lorem-service' >Lorem ipsum dolor sit amet, elit. Id ab commodi magnam.</p>
                        </div>
                    </div>


                </div>
            </div>
        )
    }

    return (
        <div>
            <div>{renderServiceWhatWeDo()}</div>
            <div>{renderOurService()}</div>
        </div>
    )
}
