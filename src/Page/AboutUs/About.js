import React from 'react'
import "./AboutUs.css"

export default function About() {

    let renderOurInfo = () => {
        return (
            <div className='flex '>
                <div className=' aboutUsOur' >
                    <p className='aboutOur' >OUR INFO</p>
                    <p className='aboutField' >We Give You Complete  <br /> Control Of Your Poultry  Fields.</p>
                    <br />
                    <p className='AboutTextLorem' >Lorem ipsum viverra feugiat. Pellen tesque libero ut justo, <br /> ligula. Semper at tempufddfel. Lorem ipsum dolor sit amet <br /> consectetur adipisicing elit. Non quae, fugiat.</p>


                    <div className='flex QualityAbout ' >
                        <div className='flex iconAndTextOur '  >
                            <i className=" iconOurAbout1 fa-solid fa-user-shield" />

                            <div>
                                <p className='textOut2About1' >Quality Matters</p>
                                <p className='textOut2About2' >Lorem ipsum viverra feugiat libero.</p>
                            </div>

                        </div>
                        <div className='flex iconAndTextOur '  >
                            <i className=" iconOurAbout1 fa-sharp fa-solid fa-globe" />

                            <div>
                                <p className='textOut2About1' >Worldwide Service</p>
                                <p className='textOut2About2' >Lorem ipsum viverra feugiat libero.</p>
                            </div>

                        </div>


                    </div>
                </div>

                <div className='' >
                    <div className='imgAboutOur1' >

                        <img className='imgAboutOur' src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/ab.jpg" alt="" />

                    </div>

                </div>
                <div className=' text-center text-img-our-about' >
                    <div className='text-16' >
                        <p className='text-16-1' >16+</p>
                        <p className='text-16-2' >YEARS OF EXPERIENCE</p>
                    </div>
                </div>


            </div>
        )



    }
    let renderNumber = () => {
        return (
            <div className='flex number-about' >
                <div className='number-text-about' >
                    <p className='text-number-about1' >2500</p>
                    <p className='text-number-about2' >Products</p>

                </div>
                <div className='number-text-about' >
                    <p className='text-number-about1' >1500</p>
                    <p className='text-number-about2' >Expert Farmers</p>

                </div> <div className='number-text-about' >
                    <p className='text-number-about1' >1280</p>
                    <p className='text-number-about2' >Business Success</p>

                </div> <div className='number-text-about' >
                    <p className='text-number-about1' >3020</p>
                    <p className='text-number-about2' >Happy Clients</p>

                </div>


            </div>
        )
    }
    let renderComeChief = () => {
        return (
            <div className='flex' >
                <div className='imgBgChief' >
                    <div className='imgChief' >
                        <img className='imgChief1' src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/banner1.jpg" alt="" />
                        <p className='text-button-chief' >The Chicken Always Comes <br /> Chief.</p>
                        <button className='button-Chief' >Read Me</button>
                    </div>
                </div>
                <div className='imgBgChief' >
                    <div className='imgChief' >
                        <img className='imgChief1' src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/banner2.jpg" alt="" />
                        <p className='text-button-chief' >Natures Intention With <br /> Chicken.</p>
                        <button className='button-Chief' >Read Me</button>
                    </div>
                </div>

            </div >
        )
    }
    let renderContactAbout = () => {
        return (
            <div>
                <div className='imgContactAbout' >
                    <img className='img-contact-about' src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/banner1.jpg" alt="" />
                </div>
                <div className='flex'>
                    <div>
                        <p className=' text-button-contact-about' >OUR INVENTION</p>
                        <p className=' text-Our-contact-about' >Our Invention Is Always The <br /> Best</p>
                    </div>
                    <div className=''>
                        <button className='button-our-about1' >Read Me</button>
                        <button className='button-our-about2'>Contact Us</button>
                    </div>
                </div>
            </div>
        )
    }
    let renderOurTeamAbout = () => {
        return (
            <div className='mt-20 our-team-about' >
                <div className='text-center' >
                    <p className='text-ourteam-about1' >
                        OUR TEAM
                    </p>
                    <p className='text-ourteam-about2' >Who Worked With Us.</p>
                </div>
                <div className='grid grid-cols-3 gap-5 p-20 ' >
                    <div className='img-ourteam-about'  >
                        <img src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/team1.jpg" alt="" />
                        <div className='icon-ourteam-about flex'  >
                            <div className='i1' >
                                <i class="fa-brands fa-facebook"></i>
                            </div>
                            <div className='i2' >
                                <i class="fa-brands fa-square-twitter"></i>
                            </div>
                            <div className='i3'>
                                <i class="fa-brands fa-square-instagram"></i>
                            </div>
                        </div>
                        <div className='text-center mt-3' >
                            <p className='text-ourteam-about' >Jack Peters</p>
                            <p className='text-lg text-gray-800' >Lorem Ipsum</p>
                        </div>
                    </div>

                    <div className='img-ourteam-about2'  >
                        <img src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/team3.jpg" alt="" />
                        <div className='icon-ourteam-about2 flex'  >
                            <div className='i4' >
                                <i class="fa-brands fa-facebook"></i>
                            </div>
                            <div className='i5' >
                                <i class="fa-brands fa-square-twitter"></i>
                            </div>
                            <div className='i6'>
                                <i class="fa-brands fa-square-instagram"></i>
                            </div>
                        </div>
                        <div className='text-center mt-3' >
                            <p className='text-ourteam-about' >Lawrence Petrie</p>
                            <p className='text-lg text-gray-800' >Lorem Ipsum</p>
                        </div>
                    </div>
                    <div className='img-ourteam-about3'  >
                        <img src="https://wp.w3layouts.com/poultry-field/wp-content/themes/paid-files/poultry-field/assets/images/team2.jpg" alt="" />
                        <div className='icon-ourteam-about3 flex'  >
                            <div className='i7' >
                                <i class="fa-brands fa-facebook"></i>
                            </div>
                            <div className='i8' >
                                <i class="fa-brands fa-square-twitter"></i>
                            </div>
                            <div className='i9'>
                                <i class="fa-brands fa-square-instagram"></i>
                            </div>
                        </div>
                        <div className='text-center mt-3' >
                            <p className='text-ourteam-about' >Anna Phillips</p>
                            <p className='text-lg text-gray-800' >Lorem Ipsum</p>
                        </div>
                    </div>


                </div>
            </div>
        )
    }


    return (
        <div>
            <div className='OurAbout1' >
                {renderOurInfo()}


            </div>
            <div className='numberAbout' >
                {renderNumber()}
            </div>
            <div>
                {renderComeChief()}
            </div>
            <div>{renderContactAbout()}</div>
            <div>
                {renderOurTeamAbout()}
            </div>
            <div>

            </div>
        </div>
    )
}
