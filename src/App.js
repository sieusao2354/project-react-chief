import './App.css';
import { BrowserRouter, Route, Router, Routes } from 'react-router-dom';
import Layout from './HOC/Layout';
import HomePage from './Page/HomPage/HomePage';
import About from './Page/AboutUs/About';
import ServicePage from './Page/ServicePage/ServicePage';
import BlogPages from './Page/BlogPages/BlogPages';
import OtherLayout from './HOC/OtherLayout';

function App() {
  return (
    <div className="">

      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Layout Component={HomePage} />} />
          <Route path='about' element={<Layout Component={About} />} />
          <Route path='service' element={<Layout Component={ServicePage} />} />
          <Route path='blogpage' element={<Layout Component={BlogPages} />} />
          <Route path='404err' element={<OtherLayout Component1={OtherLayout} />} />
        </Routes>


      </BrowserRouter>


    </div>
  );
}

export default App;
